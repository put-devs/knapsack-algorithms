/*
  A collection of functions implementing popular Knapsack Problem solving algorithms.
*/


// Non-export functions
//Permutations in one line
//  permutations = (n) => (n<1)? [[]] : permutations(n-1).map((el)=>el.concat(0)).concat(permutations(n-1).map((el)=>el.concat(1))); 
function permutations(n){
  if (n<1) return [[]];
  let easier = permutations(n-1);
  return easier.map((el)=>el.concat(0)).concat(easier.map((el)=>el.concat(1)));
}

function findDynamicSolution(knapsack, x, y, matrix){
  let current = knapsack[y-1];
  let solutions = [];
  if (y < 1) return solutions;
  if (x-current.weight >= 0 &&
  matrix[x-current.weight][y-1]+current.price === matrix[x][y]){
    solutions = solutions.concat(current).concat(findDynamicSolution(knapsack, x-current.weight, y-1, matrix));
  }
  else if (matrix[x][y-1] === matrix[x][y])
      solutions = solutions.concat(findDynamicSolution(knapsack, x, y-1, matrix));
  return solutions;
}

// Export functions
let knapsackEx = {};

knapsackEx.validate = () => {
  return 'knapsack.js is functional';
}

knapsackEx.generateData = (elementCount) => {
  let min = 1;
  let max = 30;
  let knapsack = [];
  for (let i=0;i<elementCount;++i){
    knapsack.push(
      {
      weight: Math.floor((Math.random() * max) + min),
      price: Math.floor((Math.random() * max) + min)
      });
  }
  return knapsack;
}

knapsackEx.greedy = (knapsack, size) => {
  knapsack = knapsack.map((element, ix) => {
    return {price: element.price, weight: element.weight, index: ix};
  });
  knapsack.sort((a,b) => {
    let q1 = a.price / a.weight;
    let q2 = b.price / b.weight;
    if (q1 > q2) return -1;
    if (q2 > q1) return 1;
    return 0;
  });
  let solution = [];
  let indexes = [];
  let weightSum = 0;
  let sum = 0;
  for (let i=0;i<knapsack.length;++i){
    if (size >= knapsack[i].weight) {
      solution.push(knapsack[i]);
      indexes.push(knapsack[i].index);
      sum += knapsack[i].price;
      weightSum += knapsack[i].weight;
      size -= knapsack[i].weight;
    }
  }
  return {solution: solution, priceSum: sum, weightSum: weightSum, indexes: indexes};
}

knapsackEx.bruteForce = (knapsack, size) => {
  let possibilities = permutations(knapsack.length);
  let priceSum, weightSum, solution, indexes;
  //let output = [];
  let best = {priceSum: -Infinity};
  possibilities.forEach((possibility) => {
    priceSum = 0;
    weightSum = 0;
    solution = [];
    indexes = [];
    possibility.forEach((taking, index)=>{
      if (!taking) return;
      priceSum += knapsack[index].price;
      weightSum += knapsack[index].weight;
      solution.push(knapsack[index]);
      indexes.push(index);
    });
    //output.push({priceSum: priceSum,
    //  weightSum: weightSum, solution: solution});
    if (weightSum <= size && priceSum > best.priceSum)
      best = {solution: solution,
        priceSum: priceSum,
        weightSum: weightSum,
      indexes: indexes};
  });
  return best;
}

knapsackEx.printMatrix = (matrix) => {
  let line;
  for (let y=0;y<matrix[0].length;++y){
    line = '[ ';
    for (let x=0;x<matrix.length; ++x){
      line += `${matrix[x][y]}, `
    }
    line = line.substring(0, line.length - 1);
    line += ' ]';
    console.log(line);
  }
}

knapsackEx.dynamic = (knapsack, size) =>{
  let matrix = Array.from(Array(size+1), () => new Array(knapsack.length+1).fill(0));
  let leave, take;
  let current;
  for (let y=1;y<=knapsack.length;++y){
    for (let x=1;x<=size; ++x){
      current = knapsack[y-1];
      leave = matrix[x][y-1];
      if (x-current.weight > -1)
        take = matrix[x-current.weight][y-1] + current.price;
      else take = -Infinity;
      matrix[x][y] = Math.max(leave, take);
    }
  }
  //knapsackEx.printMatrix(matrix);
  let knapWithIndexes = knapsack.map((el, ix) => {
    return {weight: el.weight, price: el.price, index: ix};
  });
  let solution = findDynamicSolution(knapWithIndexes, size, knapsack.length, matrix);
  let priceSum = matrix[size][knapsack.length];
  let weightSum = 0;
  let indexes = [];
  solution.forEach((el) => {
    weightSum += el.weight;
    indexes.push(el.index);
  });
  return {solution: solution, priceSum: priceSum, weightSum: weightSum, indexes: indexes};
}

module.exports = knapsackEx;