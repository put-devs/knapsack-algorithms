class Menu{
  constructor(functionsArray, textsArray, options={}){
    /*
      The `options` object recognizes the following properties:

      > clear: boolean (true) - Determines whether to clear the console before printing the menu.
      > style: number (0) - Currently without any impact
      > title: string ('') - The text displayed on the top of the whole menu
      > extraInfo: function (() => {}) - An optional function for displaying addtional information below the title
      > subtitle: string ('') - The text displayed before the possible selections
      > getInput: function (() => {setTimeout(() => {}, 1000)}) - The function run to get input from the user.
      > validate: function (see `Menu` constructor for implementation) - The function run to translate input data to usable form and reject
        wrong inputs.
          Expected to be the translated version of input or `false` if input was incorrect.
      > inputError: string ('Input incorrect!') - The text displayed if the validate function's output was of logical value `false`.
      > exitCode : any ('exitMenu') - The output from a function that causes exiting the menu loop.
          For example if it is a string 'exitMenu', and a function selected from the menu returns the string 'exitMenu' the menu loop is stopped.
      WARNING!
        `getInput`'s default value does not get any input! It's just a placeholder function.
          For a quick start try the prompt-sync package.
            You could require it: `const prompt = require('prompt-sync')();`
            and set the getInput function to prompt, the third argument to the Menu constructor
            could then look like this: `{getInput: prompt}`.
          The prompt-sync package was not created by me! The copyright notice of prompt-sync follows:
            Copyright (c) 2014-2019 Paolo Fragomeni & David Mark Clements
          For more information visit: https://www.npmjs.com/package/prompt-sync
    */

    this.functions = functionsArray;
    this.texts = textsArray;

    /* Options
        Default values are set here */
    this.clear = true;
    this.style = 0;
    this.title = '';
    this.extraInfo = () => {};
    this.subtitle = '';
    this.getInput = () => {setTimeout(() => {}, 1000)};
    this.validate = (input) => {
      if (!(/^[0-9]*$/.test(input))) return false;
      let back = input;
      input = parseInt(input, 10);
      if (isNaN(input)) return false;
      if (input < 0 || input > functionsArray.length-1) return false;
      return input;
    }
    this.inputError = 'Input incorrect!';
    this.exitCode = 'exitMenu';
    /* Default values have been set.
        Now overwriting using the `options` object given to this constructor */
    Object.assign(this, options);
    
    /* Creating the textContent for the menu */
    this.textContent = '';
    for (let i=0; i < this.texts.length; ++i){
      if (i+1 < this.texts.length){
        this.textContent += `${i+1}. ${this.texts[i]}\n`;
        continue;
      }
      this.textContent += `0. ${textsArray[i]}`;
    }
    this.exit = false;
    this.error = false;
  }
  print(){
    if (this.clear) console.clear();
    if (this.title !== '') console.log(`${this.title}`);
    this.extraInfo();
    if (this.subtitle !== '') console.log(`${this.subtitle}`);
    console.log(this.textContent);
    if (this.error) console.log(this.error);
  }
  select(option, args=[]){
    if (this.functions[option](...args) === this.exitCode) this.exit = true;
  }
  loop(){
    this.exit = false;
    this.error = false;
    let input;
    while (!this.exit){
      this.print();
      input = this.getInput();
      input = this.validate(input);
      if (input !== false) this.select(input);
      else this.error = this.inputError;
    }
  }
}

module.exports = Menu;