const validate = require('validate');
const Menu = require('menu');
const prompt = require('prompt-sync')();
const knapsack = require('knapsack');
const fs = require('fs');
const execSync = require('child_process').execSync;

/////////          -- VARIABLES --          /////////
let userData = [];      // connections from the user / generator / file
let tempUserData = false;
let knapsackSize = '-';

let settings = {
  indexing: true,
  showCaption: true, 
  displayStyle: 2
};

/////////          -- TESTING FUNCTIONS --          /////////
function measure(fun, ...args){
  let t1 = performance.now();
  const out = fun(...args);
  let t2 = performance.now();
  let time = Math.floor((t2 - t1)*1000)/1000;
  return {out: out, time: time}
}

/////////          -- MENU FUNCTIONS --          /////////
function showData(knapData, indexing=false, showCaption=false, otherIndexes=[]){
  //knapData = list of objects representing knapsack items
  //indexing = bool which determines whether to display item indexes
  //showCaption = bool which determines whether to display knapsack size in the first row
  //otherIndexes = array - if element otherIndexes[ix] exists it is used instead of regular index (ix)
  if (knapData.length > 19) {
    console.log(`| Plecak: [... ${knapData.length} elementów ...]`);
    return;
  }
  if (knapData.length < 1) {
    console.log(`│ Plecak jest pusty!\n`);
    return;
  }
  let kns = knapsackSize.toString().length;

  let l1,l2, spaces;
  let hat       = '┌─────────'
  let caption   = '│ Plecak  │'
  let overIndex;
  if (showCaption) overIndex = '├─────────';
  else             overIndex = '┌─────────';
  let indexBar  = '│ Indeks  │';
  let topBar;
  if (indexing || showCaption) topBar    = '├─────────';
  else          topBar    = '┌─────────';
  let priceBar  = '│ Wartość │';
  let midBar    = '├─────────';
  let weightBar = '│ Waga    │';
  let botBar    = '└─────────';
  let topBarSep = (indexing)? '┼' : '┬';
  let displayIndex;
  knapData.forEach((data, ix) => {
    displayIndex = (otherIndexes.length > ix)? `${otherIndexes[ix]}` : `${ix}`;
    l1 = data.price.toString().length;
    l2 = data.weight.toString().length;
    spaces = Math.max(l1, l2) + 1;
    if (indexing) spaces = Math.max(l1, l2, displayIndex.toString().length) + 1;
    if (ix === 0){
      if (showCaption) {
        spaces = Math.max(spaces, (kns+1));
        caption += ' '.repeat(spaces-kns) + `${knapsackSize} │`;
      }
      hat += '┬' + '─'.repeat(spaces+1);
      overIndex += (showCaption)? '┼' : '┬';
      topBar += (indexing || showCaption)? '┼' : '┬';
    }
    else if (ix === 1) {
      overIndex += (showCaption)? '┼' : '┬';
      topBar += (indexing || showCaption)? '┼' : '┬';
    }
    else{
      overIndex += '┬';
      topBar  += topBarSep;
    }
    overIndex  += '─'.repeat(spaces+1);
    indexBar += ' '.repeat(spaces-displayIndex.toString().length);
    indexBar += `${displayIndex} │`;
    topBar  += '─'.repeat(spaces+1);
    midBar  += '┼' + '─'.repeat(spaces+1);
    botBar  += '┴' + '─'.repeat(spaces+1);
    priceBar += ' '.repeat(spaces-l1) + `${data.price} │`;
    weightBar += ' '.repeat(spaces-l2) + `${data.weight} │`;
  });
  if (showCaption && knapData.length < 2) overIndex += '┤';
  else overIndex += '┐';

  hat += '┐';
  if (indexing || (showCaption && knapData.length < 2)) topBar += '┤';
  else topBar += '┐';
  
  midBar += '┤';
  botBar += '┘';
  if (showCaption){
    console.log(hat)
    console.log(caption);
  }
  if (indexing){
    console.log(overIndex);
    console.log(indexBar);
  }
  console.log(topBar);
  console.log(priceBar);
  console.log(midBar);
  console.log(weightBar);
  console.log(botBar);
}

let algorithmMenuFunctions = [
  () => 'exitMenu',
  () => {
    console.clear();
    console.log('-- ALGORYTM ZACHŁANNY --');
    if (!userData.length){
      console.log('Aby uruchomić algorytm wstaw elementy do plecaka!');
      prompt();
      return;
    }
    if (knapsackSize === '-'){
      console.log('Aby uruchomić algorytm należy ustawić jego pojemność!');
      prompt();
      return;
    }
    //showData(userData, settings.indexing, settings.showCaption);
    const {solution: solution, priceSum: priceSum, weightSum: weightSum, indexes: indexes} = knapsack.greedy(userData, knapsackSize);
    console.log(`Wybrane przedmioty:`);
    showData(solution, settings.indexing, false, indexes);
    console.log(`Suma wartości: ${priceSum}, Suma wag: ${weightSum}`);
    prompt();
  },
  () => {
    console.clear();
    console.log('-- ALGORYTM DYNAMICZNY --');
    if (!userData.length){
      console.log('Aby uruchomić algorytm wstaw elementy do plecaka!');
      prompt();
      return;
    }
    if (knapsackSize === '-'){
      console.log('Aby uruchomić algorytm należy ustawić jego pojemność!');
      prompt();
      return;
    }
    const {solution: solution, priceSum: priceSum, weightSum: weightSum,
           indexes: indexes} = knapsack.dynamic(userData, knapsackSize);
    console.log(`Wybrane przedmioty:`);
    showData(solution, settings.indexing, false, indexes);
    console.log(`Suma wartości: ${priceSum}, Suma wag: ${weightSum}`);
    prompt();
  },
  () => {
    console.clear();
    console.log('-- ALGORYTM SIŁOWY --');
    if (!userData.length){
      console.log('Aby uruchomić algorytm wstaw elementy do plecaka!');
      prompt();
      return;
    }
    if (knapsackSize === '-'){
      console.log('Aby uruchomić algorytm należy ustawić jego pojemność!');
      prompt();
      return;
    }
    const {solution: solution, priceSum: priceSum,
      weightSum: weightSum, indexes: indexes}
      = knapsack.bruteForce(userData, knapsackSize);
      console.log(`Wybrane przedmioty:`);
      showData(solution, settings.indexing, false, indexes);
      console.log(`Suma wartości: ${priceSum}, Suma wag: ${weightSum}`);
      prompt();
  }
];
let algorithmMenuTexts = ['Algorytm zachłanny', 'Algorytm dynamiczny', 'Algorytm siłowy', 'Powrót'];

let algorithmMenu = new Menu(algorithmMenuFunctions, algorithmMenuTexts, {
  title: '-- ALGORYTMY--',
  extraInfo: () => {
    showData(userData, settings.indexing, settings.showCaption);
  },
  subtitle: 'Wprowadź numer operacji i zatwierź przyciskiem ENTER:',
  inputError: 'Podaj prawidłowy numer operacji!',
  getInput: prompt
});

let dataInputMenuFunctions = [
  () => 'exitMenu',
  () => {
    let style = 0;
    while(true){
    console.clear();
    console.log('-- WPROWADZANIE DANYCH --');
    showData(userData, settings.indexing, settings.showCaption);
    console.log(`Nie wpisuj nic, aby powrócić do poprzedniego menu`);
    if (tempUserData === true) console.log(`Podaj wagę elementu:`);
    else console.log(`Podaj wartość elementu:`);
    if (style === 1 ) console.log(`Podaj prawidłową wartość!`);
    style = 0;
    option = prompt('> ')
    if (option === '') {
      if (tempUserData === true) userData.pop();
      tempUserData = false;
      break;
    }
    if ((option = validate.strToUnsignedInt(option)) === false || option === 0){
      style = 1; continue;
    }
    if (tempUserData === false){
      tempUserData = true;
      userData.push({weight: '-', price: option});
      continue;
    }
    userData[userData.length-1].weight = option;
    tempUserData = false;
  }},
  () => {
    let style = 0;
    while(true){
    console.clear();
    console.log('-- WPROWADZANIE DANYCH --');
    showData(userData, settings.indexing, settings.showCaption);
    console.log(`Nie wpisuj nic, aby powrócić do poprzedniego menu`);
    console.log(`Podaj pojemność plecaka:`);
    if (style === 1 ) console.log(`Podaj prawidłową wartość!`);
    style = 0;
    option = prompt('> ')
    if (option === '') return;
    if ((option = validate.strToUnsignedInt(option)) === false){
      style = 1; continue;
    }
    knapsackSize = option;
    return;
    }
  },
  () => userData.pop(),
  () => {userData = []; knapsackSize = '-';}
];
let dataInputMenuTexts = ['Wprowadź elementy', 'Ustaw pojemność plecaka', 'Usuń ostatni element', 'Wyczyść', 'Powrót'];
let dataInputMenu = new Menu(dataInputMenuFunctions, dataInputMenuTexts, {
  title: '-- WPROWADZANIE DANYCH --',
  extraInfo: () => {
    showData(userData, settings.indexing, settings.showCaption);
  },
  inputError: 'Podaj prawidłowy numer operacji!',
  getInput: prompt
});
let dataMenuFunctions = [
  () => 'exitMenu',
  () => dataInputMenu.loop(),
  () => {
    let option = '0';
    let style = 0;
    while(true){
      console.clear();
      console.log('-- GENEROWANIE DANYCH --');
      console.log(`Nie wpisuj nic, aby powrócić do poprzedniego menu`);
      console.log(`Nie utworzono jeszcze generatora danych!`);
      if (style > 0 ) console.log(`Podaj prawidłową wartość!`);
      style = 0;
      option = prompt('> ');
      if (option === '') return;
      if (!(option = validate.strToInt(option))){
        style = 1; continue;
      }
      //userData = graph.createConnections(option, false);
      //console.log(userData);
      prompt();
      return;
    }
  },
  () => {
    let path;
    let file;
    while(true){
      console.clear();
      console.log('-- WCZYTYWANIE PLECAKA Z PLIKU --');
      console.log(`Nie wpisuj nic, aby powrócić do poprzedniego menu`);
      console.log(`Proszę podać ścieżkę do pliku:`);
      path = prompt('> ');
      if (path === '') return;
      try {
        file = fs.readFileSync(path, { encoding: 'utf8' });
        break;
      } catch(err) {
        console.log('Błąd wczytywania. Czy plik istnieje?');
        console.log('\t' + err.message);
        console.log('\nWciśnij ENTER, aby kontynuować...');
        prompt();
      }
    }
    file = file.split('\n').map((line) => line.split(' '));
    let lineOne = file[0];

    let items = validate.strToInt(lineOne[0]);
    let size = validate.strToInt(lineOne[1]);
    if (items === false || size === false){
      console.clear();
      console.log(`Plik zawiera nieprawidłowe dane`);
      prompt();
      return;
    }
    let r, w;
    let input = [];
    
    for (let x=1;x<file.length;++x){
      r = validate.strToInt(file[x][0]);
      w = validate.strToInt(file[x][1]);
      if (r === false || w === false){
        console.clear();
        console.log(`Plik zawiera nieprawidłowe dane`);
        prompt();
        return;
      }
      input.push({weight: r, price: w});
    }
    userData = input;
    knapsackSize = size;
    if (userData.length !== items){
      console.clear();
      console.log(`Podana ilość przedmiotów jest nieprawidłowa`);
      console.log(`Ten błąd jest jednak nieznaczny i zawartość plecaka wczytano zakładając poprawność listy przedmiotów`);
      console.log('Wciśnij ENTER, aby kontynuować...');
      prompt();
      return;
    }
    console.clear();
    console.log(`Pomyślnie wczytano dane`);
    showData(userData, settings.indexing, settings.showCaption);
    console.log('Wciśnij ENTER, aby kontynuować...');
    prompt();
  }
];
let dataMenuTexts = ['Wprowadź dane z klawiatury', 'Pobierz dane z generatora', 'Pobierz dane z pliku', 'Powrót'];
let dataMenu = new Menu(dataMenuFunctions, dataMenuTexts, {
  title: '-- OPERACJE NA DANYCH --',
  /*extraInfo: () => {
    showData(userData)
  },*/
  inputError: 'Podaj prawidłowy numer operacji!',
  getInput: prompt
});

let settingsMenuFunctions = [
  () => 'exitMenu',
  () => {
    console.clear();
    settings.showCaption = !settings.showCaption;
    console.log(((settings.showCaption)? 'Włączono' : 'Wyłączono') + ' wyświetlanie pojemności plecaka');
    console.log('Podgląd:');
    showData([{price: 3, weight: 2}, {price: 2, weight: 3}], settings.indexing, settings.showCaption);
    prompt();
  },
  () => {
    console.clear();
    settings.indexing = !settings.indexing;
    console.log(((settings.indexing)? 'Włączono' : 'Wyłączono') + ' wyświetlanie indeksów');
    console.log('Podgląd:');
    showData([{price: 3, weight: 2}, {price: 2, weight: 3}], settings.indexing, settings.showCaption);
    prompt();
  }
];
let settingsMenuTexts = ['Pokazuj pojemność plecaka', 'Pokazuj indeksy', 'Cofnij'];
let settingsMenu = new Menu(settingsMenuFunctions, settingsMenuTexts, {
  title: '-- USTAWIENIA --',
  inputError: 'Podaj prawidłowy numer operacji!',
  getInput: prompt
});

let mainMenuFunctions = [
  () => 'exitMenu',
  () => dataMenu.loop(),
  () => algorithmMenu.loop(),
  () => settingsMenu.loop(),
  () => {
    console.clear();
    console.log('-- TESTOWANIE ALGORYMÓW --');
    let tries = 10;
    let elementCount = 10;
    let data;
    let testSize;
    let testData;
    let out = new Array(tries).fill({});
    //console.log('Ilość elementów;Czas zachłanny;Czas dynamiczny;Czas Brute Force');
    console.log('Press enter to proceed with element count tests.\nType anything before pressing enter to skip');
    if (prompt('> ') === '')
    for (let n=0; n<=981; ++n){
      if (n>20) n+=19;
      elementCount = n;
      testSize = n*2;
      for (let i=0;i<tries;++i){
        data = knapsack.generateData(elementCount);
        testData = [...data];
        out[i].timeGreedy = measure(knapsack.greedy, testData, testSize).time;
        testData = [...data];
        out[i].timeDynamic = measure(knapsack.dynamic, testData, testSize).time;
        if (elementCount<21){
          testData = [...data];
          out[i].timeBruteForce = measure(knapsack.bruteForce, testData, testSize).time;
        }
        console.log(`${elementCount};${out[i].timeGreedy};${out[i].timeDynamic};${out[i].timeBruteForce}`);
      }
    }
    out = new Array(tries).fill({});
    tries = 1;
    console.log('Press enter to proceed with knapsack size tests.\nType anything before pressing enter to skip');
    if (prompt('> ') === '')
    for (testSize=0; testSize<=100; ++testSize){
      elementCount = 20;
      for (let i=0;i<tries;++i){
        data = knapsack.generateData(elementCount);
        testData = [...data];
        out[i].timeGreedy = measure(knapsack.greedy, testData, testSize).time;
        testData = [...data];
        out[i].timeDynamic = measure(knapsack.dynamic, testData, testSize).time;
        testData = [...data];
        out[i].timeBruteForce = measure(knapsack.bruteForce, testData, testSize).time;
        console.log(`${testSize};${out[i].timeGreedy};${out[i].timeDynamic};${out[i].timeBruteForce}`);
      }
    }
    tries = 10;
    console.log('Press enter to proceed with knapsack size and element count tests.\nType anything before pressing enter to skip');
    if (prompt('> ') === '')
    for (testSize=0; testSize<=100; testSize+=5){
      for (elementCount=0; elementCount<=20; elementCount+=2){
        for (let i=0;i<tries;++i){
          data = knapsack.generateData(elementCount);
          testData = [...data];
          out[i].timeGreedy = measure(knapsack.greedy, testData, testSize).time;
          testData = [...data];
          out[i].timeDynamic = measure(knapsack.dynamic, testData, testSize).time;
          testData = [...data];
          out[i].timeBruteForce = measure(knapsack.bruteForce, testData, testSize).time;
          console.log(`${testSize};${elementCount};${out[i].timeGreedy};${out[i].timeDynamic};${out[i].timeBruteForce}`);
        }
      }
    }
    prompt();
  }
];

let mainMenuTexts = ['Wprowadź dane', 'Uruchom Algorytmy', 'Ustawienia', 'Testy', 'Wyjdź'];
let mainMenu = new Menu(mainMenuFunctions, mainMenuTexts, {
  title: '-- MENU --',
  inputError: 'Podaj prawidłowy numer operacji!',
  getInput: prompt
});

userData = [{price: 2, weight: 3}, {price: 5, weight: 4}, {price: 3, weight: 1}, {price: 2, weight: 2}, {price: 4, weight: 4}];
knapsackSize = 6;

mainMenu.loop();