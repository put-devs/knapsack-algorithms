/*
  Handful functions for validating data in JavaScript.
  Incorrect data returns `false`.
  Correct data returns the data in the desired data type
  or `true` if the second argument of the function is of logical value of true.
  _________________________
  A quick example of usage:

  const validate = require('validate');
  let input = '123'; // The variable input contains the data that we want to validate
  if ((input = validate.strToInt(input)) === false)
  // `input` is validated and this if's contition will be met if validation failed
    console.log('The data is incorrect!');
  else
    console.log(`The data is correct! Value: ${input}`);

*/
let validateEx = {};

validateEx.validate = () => {
  return 'validate.js is functional';
}

validateEx.strToInt = (input, justCheck=false) => {
  /* Returns an integer (or `true` if the second argument of the function
      is of logical value of true) if the input is a valid integer-containing string
    Returns false otherwise */

  if (!(/^-?[0-9]*$/.test(input))) return false;
  input = parseInt(input, 10);
  if (isNaN(input)) return false;
  if (justCheck) return true;
  return input;
}

validateEx.strToUnsignedInt = (input, justCheck=false) => {
  /* Returns an integer (or `true` if the second argument of the function
      is of logical value of true) if the input is a valid integer-containing string
    Returns false otherwise */

  if (!(/^[0-9]*$/.test(input))) return false;
  input = parseInt(input, 10);
  if (isNaN(input)) return false;
  if (justCheck) return true;
  return input;
}


module.exports = validateEx;